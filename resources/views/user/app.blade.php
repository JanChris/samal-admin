<!DOCTYPE html>
<html>
<head>
    @include('user.layout.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('user.layout.header')
    @include('user.layout.nav')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>


        <section class="content">
            @yield('main-content')
        </section>

    </div>
    @include('user.layout.footer')
</div>

@include('user.layout.script')
@yield('script')
</body>
</html>
