<!DOCTYPE html>
<html>
<head>
    @include('admin.layout.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('admin.layout.header')
    @include('admin.layout.nav')
    <div class="content-wrapper">
        <section class="content-header">
            @yield('content-header')
        </section>

        <section class="content">
            @yield('content')
        </section>

    </div>
    @include('admin.layout.footer')
</div>

@include('admin.layout.script')
@yield('script')
</body>
</html>
