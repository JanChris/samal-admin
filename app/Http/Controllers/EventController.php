<?php

namespace App\Http\Controllers;

use App\Event;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = DB::table('events as event')
            ->leftJoin('locations as loc', 'event.location_id', '=', 'loc.id')
            ->select('event.*', 'loc.name as location_name', 'loc.id as location_id')
            ->whereNull('deleted_at')
            ->orderBy('updated_at', 'desc')
            ->paginate(10);
        $location = Location::all();
        return view('admin.event.event', ['events' => $events, 'locations' => $location]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'location_id'   => 'required',
            'date'          => 'required',
            'time'          => 'required'
        ]);

        Event::create([
            'name'          => $request->name,
            'location_id'   => $request->location_id,
            'date'          => $request->date,
            'time'          => $request->time,
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('events.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $event->name            = $request->name;
        $event->location_id     = $request->location_id;
        $event->date            = $request->date;
        $event->time            = $request->time;
        $event->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('events.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return redirect(route('events.index'));
    }
}
